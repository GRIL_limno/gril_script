# **GRIL Script **

([English version](https://bitbucket.org/GRIL_limno/gril_script/src/9c327eef75dd7bbfe19ee6c640ebfd0c68574230/README_En.md?fileviewer=file-view-default)) 


Bienvenue dans le répertoire GRIL script. L'objectif de ce répertoire est de favoriser le partage de scripts et de fournir un réseau de support pour l'utilisation des différentes méthodes statistiques en recherche. 

### Quels sont les objectifs de ce répertoire?

Vous allez trouver dans ce répertoire des scripts permettant de réaliser plusieurs analyses statistiques à l'aide de différents programmes d'analyse statistique (comme R). Vous pouvez accéder aux scripts en ligne sur Bitbucket ou encore sur votre ordinateur en utilisant SourceTree. De plus, vous pouvez contacter d'autres membres du GRIL en utilisant HipChat.

* Version
* [Apprendre Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Pour commencer

Pour débuter, vous pouvez télécharger les scripts directement sur [BitBucket](https://bitbucket.org/GRIL_limno/gril_script/downloads), sans avoir ŕ vous inscrire. Par contre, pour pouvoir maintenir vos scripts ŕ jour ou encore pour participer au développement des scripts GRIL, vous devez posséder un compte BitBucket. SourceTree vous permet de télécharger facilement vos scripts sur le serveur et de mettre ŕ jour les fichier sur votre ordinateur.    

### Installer BitBucket

1. Aller Ĺ [Bitbucket](https://bitbucket.org) et créer un compte. 
2. Installer [git](https://confluence.atlassian.com/bitbucket/set-up-git-744723531.html) sur votre ordinateur.
3. Installer [SourceTree](https://www.sourcetreeapp.com/) sur votre ordinateur. 

Après avoir initialisé votre compte BitBucket et téléchargé  SourceTree, vous devez cloner le répertoire GRIL script sur votre ordinateur. 

### Clone le répertoire GRIL script 

1. Aller sur la page de [GRIL script](https://bitbucket.org/GRIL_limno/gril_script/overview)
2. Cliquer sur **Clone** dans le menu de droite
3. Cliquer **Clone in Source Tree**
4. Cliquer **Launch Application**
5. Dans Source Tree sélectionner un dossier pour installer GRIL script
6. Dans Source Tree Cliquer sur **Clone**

Aprčs avoir cloné le répertoire dans SourceTree, vous pouvez maintenant facilement ajouter ou télécharger des scripts.


### Ajouter/ modifier un ficher de GRIL script

1. Sauver le fichier dans votre dossier BitBucket local
2. Dans la section **Unstaged files** , cliquer la boite à côté du fichier que vous avez modifié
3. Cliquer sur **Commit** dans le menu du haut
4. Décrire ce que vous avez changé en utilisant le format suivant: 
    * [nomdufichier] (ex. [RDA.r])
    * Sur la mÄme ligne inclure une courte description de ce que vous avez changé (ex. J'ai ajouté une figure)
    * Et une longue description/justification sur les lignes suivantes
5. Cliquer sur **Commit** dans la fenÄtre
6. Cliquer sur **Push** dans le menu du haut
7. Cliquer sur **ok**

## Aidez-moi j'ai un problčme!

 Maintenant que vous avez accès au répertoire GRIL script vous pouvez commencer à utiliser les scripts des autres membres du GRIL. Si jamais vous avez des problčmes avec un script vous pouvez les régler de deux façons. 

**Premičrement:** Vous pouvez joindre les autres membres avec HipChat qui est un forum de discussions en ligne. 

Pour utiliser HipChat vous devez avoir un  [compte](https://www.hipchat.com/invite/557670/19fd6dfb681a58ea11742fc82192bcdd?utm_campaign=company_room_link).


**Deuxièmement:** Pour les problčmes plus grave, vous pouvez les reporter directement sur BitBucket. Vous devriez signaler un problème dans les situations suivantes:
* Il a a un bug dans un script
* Vous avez une question Ĺ propos d'un script
* Vous avez une demande 


Pour signaler un problème:

1. Cliquer sur **Issues** dans le menu de droite
2. Cliquer sur **Create issue** (en haut Ĺ droite)
3. Ăcrire un titre et décrire le problÄme en dĂŠtails
4. Si vous voulez vous pouvez assigner le problÄme Ĺ un utilisateur en particulier 
5. Sélectionner le type de problème
6. Cliquer sur **Create issue**
 
## Contribution Guidelines ##

As the GRIL is a large community, many members may have scripts for methods that are not already included in the respository. We welcome contributions for:

* Writing tests
* Code review
* Other guidelines