#### To submit a R script to the group please use the following format
1. File name should be meaningful and end in .R

2. Fill the information section

```
####################################################################
# Author:
#  Full Name, email adress
# Date:
#  year/month/day
# Description:
#  Add a clear description of our script
#
####################################################################
```
3. Empty R memory

```
rm(list=ls(all=TRUE))

```

4. Load only needed packages (should be available on CRAN or specify if otherwise)

```
library(vegan)
library(car)

```

5. Your script should be independant of external data. 

  * Use [R datasets](https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/00Index.html)
    
    ```
    data(varechem)
    ```
    
  * Create an example dataset
    
    ```
    T=c(15,20,12,35)
    pH=c(5.6,6.8,3.4,4.5)
    env=cbind(T,pH)
    ```
    

###  Good Practices in R Programming
* variable.name is preferred, variableName is accepted
* Use <-, not =, for assignment.
* Place spaces around all infix operators (=, +, -, <-, etc.)
* Comments should explain the why, not the what



For more see [Advanced R](http://adv-r.had.co.nz/Style.html)


