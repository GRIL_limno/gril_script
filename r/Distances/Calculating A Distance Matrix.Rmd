---
title: "PhD Code"
author: "G. M."
date: "March 3, 2016"
output: html_document
---
# How to Calculate a Distance Matrix
First convert the coordinates file to a numeric matrix. This function is a stock function in R.
```{r}
coord <- read.csv("coord.csv")
DataMatrix <- data.matrix(coord)
```

If your coordinates file has headings (i.e. site names for rows, and lat/lon for column headers) use instead:
```{r}
coord <- read.csv("coord.csv")
DataMatrix <- data.matrix(coord, rownames.force=TRUE)
```

Next, calculate the distance matrix. This function is part of the SpatialTools package.
```{r}
library("SpatialTools", lib.loc="~/Library/R/3.2/library")
DistMatrix <- dist1(DataMatrix)
```

Then, in RStudio, in the environment window, click on DistMatrix, (or use View(DistMatrix)) and you can export or copy your distance matrix from there. Remember to visually inpsect your coordinates file and your datamatrix before proceeding onto the next step.

Summary example:
```{r}
scheffcoord <- read.csv("Scheff.csv")
scheffmatrix <- data.matrix(Scheffcoord, rownames.force=TRUE)
scheffdist <- dist1(scheffmatrix)
View(scheffdist)
```
